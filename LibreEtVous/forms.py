from django import forms
from registration.forms import RegistrationForm
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _


class SignInForm(RegistrationForm):
    """
    Manage user signin (creation) in form.
    Overload of registration.forms.RegistrationForm
    """
    first_name = forms.CharField(max_length=30,
                                 label='Prénom',
                                 required=True)
    last_name = forms.CharField(max_length=30,
                                label='Nom',
                                required=True)

    class Meta:
        model = User
        fields = ('username',
                  'first_name',
                  'last_name',
                  'email',
                  'password1',
                  'password2',)
