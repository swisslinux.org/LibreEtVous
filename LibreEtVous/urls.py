"""LibreEtVous URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.flatpages import views as flatpages_views
from . import views as core_view

urlpatterns = [
    url(r'^$',
        flatpages_views.flatpage,
        {'url': '/index/'},
        name='site-index'),
    url(r'^accounts/register/$',
        core_view.SignInView.as_view(),
        name='signin'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^events/', include('kiss_events.urls')),
    url(r'^contact/', include('kiss_contact_us.urls')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^admin/', admin.site.urls),
]
