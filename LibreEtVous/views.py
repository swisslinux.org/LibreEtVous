from django.views.generic import base, edit
from django.contrib.auth import login
from registration.backends.default.views import RegistrationView
from django.urls import reverse_lazy

from . import forms


class SignInView(RegistrationView):
    """
    View used for Signin (registration)
    Overload of registration.views.RegistrationView
    """
    form_class = forms.SignInForm
