from django.contrib import admin
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from .models import EventType, Duration, Event, ExternalLink


# Admin list filters
class IsScheduledListFilter(admin.SimpleListFilter):
    """Filter to get events scheduled or not"""
    title = _('scheduled')
    parameter_name = 'scheduled'
    YES_CODE = 'yes'
    NO_CODE = 'no'

    def lookups(self, request, model_admin):
        """Returns the list of options"""
        return (
            (self.YES_CODE, _('yes')),
            (self.NO_CODE, _('no')),
        )

    def queryset(self, request, queryset):
        """Returns the filtered queryset based on the option"""
        if self.value() == self.YES_CODE:
            return queryset.exclude(
                schedule=None
            )
        if self.value() == self.NO_CODE:
            return queryset.filter(
                schedule=None
            )


class ExternalLinkInline(admin.TabularInline):
    """How the ExternalLink model can be manage inline from admin"""
    model = ExternalLink


# Admin details and list settings
@admin.register(EventType)
class EventTypeAdmin(admin.ModelAdmin):
    "How the EventType model can be manage from admin"


@admin.register(Duration)
class DurationAdmin(admin.ModelAdmin):
    "How the Duration model can be manage from admin"


@admin.register(ExternalLink)
class ExternalLinkAdmin(admin.ModelAdmin):
    """How the ExternalLink model can be manage from admin"""


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    "How the Event model can be manage from admin"
    list_display = (
        'title',
        'type',
        'duration',
        'contact',
        'authors_list',
        'status',
        'schedule',
    )
    list_editable = (
        'status',
    )
    list_filter = (
        'type',
        IsScheduledListFilter,
        'status',
        'duration'
    )
    search_fields = (
        'title',
        'contact__username',
        'authors_list',
        'abstract'
    )
    actions = (
        'mark_as_accepted',
        'mark_as_not_accepted',
    )
    fieldsets = (
        (
            _('Informations'),
            {
                'fields': (
                    'title',
                    'authors_list',
                    'contact',
                    ('type', 'duration'),
                    'abstract',
                    'note_for_organizers',
                )
            }
        ),
        (
            _('Programming'),
            {
                'fields': (
                    'status',
                    ('place', 'schedule'),
                )
            }
        ),
        (
            _('Recordings'),
            {
                'fields': (
                    'video_recording_url',
                    'audio_recording_url',
                )
            }
        ),
        (
            _('Attachments'),
            {
                'fields': (
                    'slides_and_files_url',
                )
            }
        ),
    )
    inlines = (
        ExternalLinkInline,
    )

    def view_on_site(self, obj):
        "View event on website"
        return reverse('kiss_events:AcceptedEventDetail',
                       args=[str(obj.id)])

    def mark_as_accepted(self, request, queryset):
        "Mark selected events as accepted"
        rows_updated = queryset.update(status=Event.ACCEPTED_CODE)
        if rows_updated == 1:
            message_bit = _('event was successfully marked as accepted')
        else:
            message_bit = _('events were successfully marked as accepted')
        self.message_user(
            request,
            _('{} {}.').format(
                rows_updated,
                message_bit,
            )
        )
    mark_as_accepted.short_description = _(
        'Mark selected events as accepted'
    )

    def mark_as_not_accepted(self, request, queryset):
        "Mark selected events as not accepted"
        rows_updated = queryset.update(status=Event.NOT_ACCEPTED_CODE)
        if rows_updated == 1:
            message_bit = _('event was successfully marked as not accepted')
        else:
            message_bit = _('events were successfully marked as not accepted')
        self.message_user(
            request,
            _('{} {}.').format(
                rows_updated,
                message_bit,
            )
        )
    mark_as_not_accepted.short_description = _(
        'Mark selected events as not accepted'
    )
