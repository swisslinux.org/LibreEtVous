from django.views.generic import DetailView, ListView, base, edit
from django.conf import settings
from django.http import Http404
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Event


class AcceptedEventDetailView(DetailView):
    "Show public informations about an accepted event"
    model = Event

    def get_object(self, queryset=None):
        "To only show an accepted event"
        event = super(AcceptedEventDetailView,
                      self).get_object(queryset=queryset)
        if not event.is_accepted():
            raise Http404()
        return event


class AcceptedEventsListView(ListView):
    "Show the list of accepted events, sorted by title"
    model = Event
    queryset = Event.objects.filter(status='Accepted').order_by('title')

    def get_context_data(self, **kwargs):
        """
        Get the list of accepted and scheduled event.
        Grouped by day.
        """
        context = super(AcceptedEventsListView, self).get_context_data(
            **kwargs
        )
        try:
            program_in_prep = settings.PROGRAM_IN_PREPARATION
            context['program_in_preparation'] = program_in_prep
        except Exception:
            context['program_in_preparation'] = True
        return context


class ScheduleView(base.TemplateView):
    """
    Show the Schedule of the meeting.

    You get all the events, grouped by date, as a dictionnary in the context
    given to the template.
    This dictionary is called 'schedule'. It has dates (datetime.date)
    as keys and corresponding events (querrysets) as values.

    """
    template_name = 'kiss_events/schedule.html'

    def get_context_data(self, **kwargs):
        """
        Get the list of accepted and scheduled event.
        Grouped by day.
        """
        context = super(ScheduleView, self).get_context_data(**kwargs)
        # See if the program is in preparation
        try:
            program_in_prep = settings.PROGRAM_IN_PREPARATION
            context['program_in_preparation'] = program_in_prep
        except Exception:
            context['program_in_preparation'] = True
        schedule = {}
        # Get dates in the schedule
        events_schedules = Event.objects.filter(
            status='Accepted',
        ).values_list(
            'schedule', flat=True,
        )
        events_dates = set((schedule.date()
                            for schedule in events_schedules
                            if schedule))
        events_dates = sorted(events_dates)
        for ev_date in events_dates:
            group_events = Event.objects.filter(
                schedule__date=ev_date,
                status='Accepted',
            ).order_by('schedule')
            schedule[ev_date] = group_events
        context['schedule'] = schedule
        return context


class EventSubmitView(LoginRequiredMixin, edit.CreateView):
    "Let user submit an event"
    login_url = reverse_lazy('auth_login')
    model = Event
    fields = [
        'title',
        'authors_list',
        'type',
        'duration',
        'abstract',
        'note_for_organizers',
    ]
    success_url = reverse_lazy('kiss_events:EventSubmitSuccess')

    def form_valid(self, form):
        """
         Overload to indicate the user id as contact of the
         submited event.
        """
        form.instance.contact = self.request.user
        return super(EventSubmitView, self).form_valid(form)


class EventSubmitSuccessView(base.TemplateView):
    "If user succefully submited an event"
    template_name = 'kiss_events/event_form_success.html'
