from django.conf.urls import url

from . import views


app_name = 'kiss_events'
urlpatterns = [
    url(r'^$',
        views.ScheduleView.as_view(),
        name='AcceptedEventsSchedule'),
    url(r'^(?P<pk>[0-9]+)/$',
        views.AcceptedEventDetailView.as_view(),
        name='AcceptedEventDetail'),
    url(r'^a-z/$',
        views.AcceptedEventsListView.as_view(),
        name='AcceptedEventsList'),
    url(r'^submit/$',
        views.EventSubmitView.as_view(),
        name='EventSubmit'),
    url(r'^submit/success$',
        views.EventSubmitSuccessView.as_view(),
        name='EventSubmitSuccess')
]
