from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class KissEventsConfig(AppConfig):
    name = 'kiss_events'
    verbose_name = _('events')
