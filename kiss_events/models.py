from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from kiss_places.models import Place


class EventType(models.Model):
    "The type of an event"
    name = models.CharField(
        verbose_name=_('name'),
        max_length=255,
    )
    description = models.TextField(
        verbose_name=_('description'),
        blank=True,
    )

    class Meta:
        ordering = ('name',)
        verbose_name = _('event type')
        verbose_name_plural = _('event types')

    def __str__(self):
        return self.name


class Duration(models.Model):
    "How many time for an event"
    minutes = models.IntegerField(
        verbose_name=_('minutes'),
    )

    class Meta:
        ordering = ('minutes',)
        verbose_name = _('duration')
        verbose_name_plural = _('durations')

    def __str__(self):
        return str(self.minutes)


class Event(models.Model):
    "A Event (talk, a workshop, etc), with all his informations"
    ACCEPTED_CODE = 'Accepted'
    NOT_ACCEPTED_CODE = 'Not Accepted'
    CANCELLED_CODE = 'Cancelled'
    WAITING_CODE = 'Waiting'
    SUBMITTED_CODE = 'Submitted'
    STATUS = (
        (ACCEPTED_CODE, _('Accepted')),
        (NOT_ACCEPTED_CODE, _('Not Accepted')),
        (CANCELLED_CODE, _('Cancelled')),
        (WAITING_CODE, _('Waiting')),
        (SUBMITTED_CODE, _('Submitted')),
    )
    DEFAULT_STATUS = SUBMITTED_CODE
    title = models.CharField(
        verbose_name=_('title'),
        max_length=255
    )
    authors_list = models.CharField(
        verbose_name=_('authors list'),
        max_length=255,
        help_text=_(
            'Separate authors with comma.'
        )
    )
    contact = models.ForeignKey(
        User, verbose_name=_('contact'),
        on_delete=models.CASCADE,
        help_text=_(
            'Who submit this event.'
        )
    )
    type = models.ForeignKey(
        EventType, verbose_name=_('type'),
        on_delete=models.CASCADE,
        help_text=_(
            'The kind of this event.'
        )
    )
    duration = models.ForeignKey(
        Duration, verbose_name=_('duration (minutes)'),
        on_delete=models.CASCADE
    )
    abstract = models.TextField(verbose_name=_('abstract'))
    note_for_organizers = models.TextField(
        verbose_name=_('note for organizers'),
        blank=True, help_text=_('Private.')
    )
    status = models.CharField(
        verbose_name=_('status'),
        max_length=42, choices=STATUS,
        default=DEFAULT_STATUS
    )
    schedule = models.DateTimeField(
        verbose_name=_('schedule'),
        blank=True, null=True,
        help_text=_('When this event start')
    )
    place = models.ForeignKey(
        Place, verbose_name=_('take place at'),
        on_delete=models.SET_NULL,
        null=True, blank=True
    )
    video_recording_url = models.URLField(
        verbose_name=_('video recording url'),
        blank=True, null=True,
        max_length=255,
    )
    audio_recording_url = models.URLField(
        verbose_name=_('audio recording url'),
        blank=True, null=True,
        max_length=255,
    )
    slides_and_files_url = models.URLField(
        verbose_name=_('slides and files url'),
        blank=True, null=True,
        max_length=255,
    )

    def is_accepted(self):
        "Is this event accepted"
        return self.status == self.ACCEPTED_CODE
    is_accepted.short_description = _('is accepted')

    def is_scheduled(self):
        "Is this event scheduled"
        return True if self.schedule else False
    is_scheduled.short_description = _('is scheduled')

    is_accepted.boolean = True
    is_scheduled.boolean = True

    class Meta:
        ordering = ('title',)
        verbose_name = _('event')
        verbose_name_plural = _('events')

    def __str__(self):
        return self.title


class ExternalLink(models.Model):
    """An external link, attached to an event"""
    description = models.CharField(
        verbose_name=_('description'),
        max_length=255,
    )
    URL = models.URLField(
        verbose_name=_('URL'),
        max_length=255,
    )
    event = models.ForeignKey(
        Event,
        verbose_name=('event'),
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ('description',)
        verbose_name = _('external link')
        verbose_name_plural = _('external links')
