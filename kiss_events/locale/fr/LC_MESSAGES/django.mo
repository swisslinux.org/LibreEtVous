��    M      �  g   �      �     �     �     �     �     �     �  	   �  	   �     �     �               "     2      ?  $   `     �     �     �     �  
   �  	   �     �     �     �     �     �       	   $     .     I     a          �     �     �  P   �     �     	     	     	     $	     ;	     D	     G	     [	     h	     k	     s	     	     �	  	   �	     �	  
   �	     �	  )   �	  -   �	     
  +   !
  /   M
     }
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
                 j  !     �     �     �     �     �     �     �     �  $   �  %        2     9     J     e  7   r  ;   �     �     �     �     �     	       	   &     0  +   8     d     }     �     �     �     �  )   �                %  
   ,  [   7      �     �     �     �     �     �               !     3     7     ?     K     R     c     k  $   w     �  7   �  ;   �     $  <   1  @   n     �  	   �     �     �     �     �  	   �  
   �           $  	   +     5     ;     @     \     `     ,   /          .   4   1       J         0       *         B       F   -   6       $   H   9   
           >      &           <             A   ;       '          2   "       M      )                  !   #   I   8           3              5   ?   %                    K       G   =          D         :      C           L   @   E         7                     +      (       	    A-Z Abstract Accepted All events A-Z Attachments Audio Author(s) Cancelled Download audio recording Download video recording Duration Duration (minutes) Events schedule Informations Mark selected events as accepted Mark selected events as not accepted Not Accepted Place Private. Programming Recordings Return to Schedule Send Separate authors with comma. Slides and files Submission registered Submit an event Submitted Thanks for your submission The kind of this event. The program is being prepared Title Type Video Waiting We have registered your submission. Your event will be visible after validation. We will contact you soon. When When this event start Where Who submit this event. abstract at audio recording url authors list by contact description duration duration (minutes) durations event event type event types event was successfully marked as accepted event was successfully marked as not accepted events events were successfully marked as accepted events were successfully marked as not accepted is accepted is scheduled minutes name no note for organizers schedule scheduled slides and files url status take place at title type video recording url yes {} {}. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-05-27 22:22+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 A-Z Résumé Accepté Tous les évènements A-Z Pièces jointes Audio Auteur·e·s Annulé Télécharger l'enregistrement audio Télécharger l'enregistrement vidéo Durée Durée (minutes) Programme des événements Informations Marquer les événements sélectionnés comme acceptés Marquer les événements sélectionnés comme non acceptés Refusé Lieu Privé. Programmation Enregistrements Retourner à Programme Envoyer Séparez les auteur·e·s avec une virgule. Diapositives et fichiers Proposition enregistrée Proposer un événement Proposé Merci pour votre proposition Le type de cet événement. Le programme est en cours de préparation Titre Type Vidéo En attente Nous avons enregistré votr propositione. Votre événement sera visible après validation. Nous vous contacterons bientôt. Quand Quand cet événement commence Où Qui propose cet événement. résumé à URL d'enregistrement audio liste des auteurs par contact description durée durée (minutes) durées événement type d'événementtype d'événement types d'événement événement a été marqué avec succès comme accepté événement a été marqué avec succès comme non accepté événements événements ont été marqués avec succès comme acceptés événements ont été marqués avec succès comme non acceptés Accepté programme minutes nom non note pour les organisateurs programme programmé URL des diapositives et fichiers statut a lieu à titre type URL d'enregistrement vidéo oui {} {}. 