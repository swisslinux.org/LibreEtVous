from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class KissPlacesConfig(AppConfig):
    name = 'kiss_places'
    verbose_name = _('places')
