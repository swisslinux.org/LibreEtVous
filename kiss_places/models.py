from django.db import models
from django.utils.translation import ugettext as _


class Place(models.Model):
    "Somewhere in the universe…"
    name = models.CharField(verbose_name=_('name'), max_length=255)
    description = models.TextField(verbose_name=_('description'), blank=True)

    class Meta:
        verbose_name = _('place')
        verbose_name_plural = _('places')

    def __str__(self):
        return self.name
