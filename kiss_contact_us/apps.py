from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class KissContactUsConfig(AppConfig):
    name = 'kiss_contact_us'
    verbose_name = _('contact us')
