from django.conf.urls import url

from . import views


app_name = 'kiss_contact_us'
urlpatterns = [
    url(
        r'^$',
        views.MessageCreateView.as_view(),
        name='MessageCreate',
    ),
    url(
        r'^received/$',
        views.MessageCreateSuccessView.as_view(),
        name='MessageCreateSuccess',
    ),
]
