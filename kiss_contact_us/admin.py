from django.contrib import admin
from .models import Subject, EmailRelay, Message


# Admin details and list settings
class EmailRelayInline(admin.StackedInline):
    "In line e-mails relay management"
    model = EmailRelay
    extra = 3


@admin.register(EmailRelay)
class EmailRelayAdmin(admin.ModelAdmin):
    "How the EmailRelay model can be manage from admin"
    list_display = ('for_subject', 'to_address')
    list_filter = ('for_subject',)
    search_fields = ('for_subject', 'to_address')


@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    "How the Subject model can be manage from admin"
    search_fields = ('subject',)
    list_display = ('subject',)
    inlines = (EmailRelayInline,)


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    "How the Message model can be manage from admin"
    list_display = ('author_name', 'subject', 'date',)
    list_filter = ('subject__subject', 'date',)
    search_fields = (
        'author_name',
        'author_email',
        'subject__subject',
        'message'
    )
    readonly_fields = (
        'date',
        'email_relays_notified')
