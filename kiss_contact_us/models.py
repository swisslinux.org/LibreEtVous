from django.db import models
from django.conf import settings
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.utils import timezone

from .emails import MessageEmailNotificationRender


class Subject(models.Model):
    """The subject for sending us a message"""
    subject = models.CharField(
        max_length=255,
        verbose_name=_('subject')
    )

    class Meta:
        verbose_name = _('subject')
        verbose_name_plural = _('subjects')
        ordering = ('subject',)

    def __str__(self):
        return self.subject


class EmailRelay(models.Model):
    """E-mail addresses where relay messages, by subjects"""
    for_subject = models.ForeignKey(
        Subject,
        on_delete=models.CASCADE,
        verbose_name=_('for subject'),
    )
    to_address = models.EmailField(
        max_length=255,
        verbose_name=_('to address')
    )


class Message(models.Model):
    """A message sent to us"""
    author_name = models.CharField(
        max_length=255,
        verbose_name=_('author name'),
    )
    author_email = models.EmailField(
        max_length=255,
        verbose_name=_('author email'),
    )
    date = models.DateTimeField(
        verbose_name=_('date'),
        blank=True, null=True,
        help_text=_('When this message was sent')
    )
    subject = models.ForeignKey(
        Subject,
        on_delete=models.CASCADE,
        verbose_name=_('subject'),
    )
    message = models.TextField(
        verbose_name=_('message'),
    )
    email_relays_notified = models.BooleanField(
        default=False,
        verbose_name=_('email relays and managers notified'),
    )

    class Meta:
        verbose_name = _('message')
        verbose_name_plural = _('messages')
        ordering = ('-date', 'author_email')

    def _get_email_settings(self):
        """
        Try to get email settings in django.conf.settings
        Return True if success, False if not
        """
        try:
            self.from_email = settings.DEFAULT_FROM_EMAIL
        except AttributeError:
            return False
        try:
            self.to_emails = settings.MANAGERS
        except AttributeError:
            self.to_emails = []
        email_relays = self.subject.emailrelay_set.all()
        for relay in email_relays:
            self.to_emails.append(relay.to_address)
        if not self.to_emails:
            return False
        return True

    def save(self, *args, **kwargs):
        """Save the model
        If no date, add it from datetime.now()
        If possible, notify email relays managers by sending a
        copy of the message by email
        """
        if not self.date:
            self.date = timezone.now()
        if self._get_email_settings():
            if not self.email_relays_notified:
                email_view = MessageEmailNotificationRender(
                    author_name=self.author_name,
                    author_email=self.author_email,
                    subject=self.subject.subject,
                    message=self.message,
                )
                email_sent = send_mail(
                    subject=email_view.rendered_subject,
                    message=email_view.rendered_message,
                    from_email=self.from_email,
                    recipient_list=self.to_emails,
                )
                if email_sent:
                    self.email_relays_notified = True
        return super(Message, self).save(*args, **kwargs)

    def __str__(self):
        return '{} {} {}'.format(
            self.author_name,
            self.date,
            self.subject.subject,
        )
