from django.forms import ModelForm
from captcha.fields import CaptchaField
from django.utils.translation import ugettext as _

from .models import Message


class MessageForm(ModelForm):
    """The form to create a Message model"""
    captcha = CaptchaField()

    class Meta:
        model = Message
        fields = [
            'author_name',
            'author_email',
            'subject',
            'message',
        ]
        labels = {
            'author_name': _('Your name'),
            'author_email': _('Your email'),
        }
