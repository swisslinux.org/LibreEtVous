from django.views.generic import base, edit
from django.urls import reverse_lazy
from django.utils.translation import ugettext as _

from .models import Message
from .forms import MessageForm


class MessageCreateView(edit.CreateView):
    """Let visitor send us messages"""
    model = Message
    form_class = MessageForm
    success_url = reverse_lazy('kiss_contact_us:MessageCreateSuccess')


class MessageCreateSuccessView(base.TemplateView):
    """If user succefully submited a message"""
    template_name = 'kiss_contact_us/message_form_success.html'
