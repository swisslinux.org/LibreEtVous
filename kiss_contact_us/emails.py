from django.template.loader import render_to_string


class MessageEmailNotificationRender:
    """Render email to notify new messages to the managers"""
    template_name = 'kiss_contact_us/message_form_email.txt'
    template_subject_name = 'kiss_contact_us/message_form_email_subject.txt'

    def __init__(self, author_name='', author_email='',
                 subject='', message=''):
        """Render the given parameters with the templates"""
        self.__author_name = author_name
        self.__author_email = author_email
        self.__subject = subject
        self.__message = message
        self.rendered_subject = ''
        self.rendered_message = ''
        self.render()

    def render(self):
        """
        Render the subject and the message to be sent by email
        """
        context = {
            'author_name': self.__author_name,
            'author_email': self.__author_email,
            'subject': self.__subject,
            'message': self.__message,
        }
        self.rendered_subject = render_to_string(
            self.template_subject_name,
            context,
        ).rstrip()
        self.rendered_message = render_to_string(
            self.template_name,
            context,
        )

    @property
    def author_name(self):
        return self.__author_name

    @author_name.setter
    def author_name(self, author_name):
        self.render()
        self.__author_name = author_name

    @property
    def author_email(self):
        return self.__author_email

    @author_email.setter
    def author_email(self, author_email):
        self.render()
        self.__author_email = author_email

    @property
    def subject(self):
        return self.__subject

    @subject.setter
    def subject(self, subject):
        self.render()
        self.__subject = subject

    @property
    def message(self):
        return self.__message

    @message.setter
    def message(self, message):
        self.render()
        self.__message = message
