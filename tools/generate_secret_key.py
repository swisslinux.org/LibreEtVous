import string
import random

authorized_chars = string.ascii_letters+string.digits+string.punctuation
key = ''
for i in range(50):
    key += random.SystemRandom().choice(authorized_chars)
print(key)
